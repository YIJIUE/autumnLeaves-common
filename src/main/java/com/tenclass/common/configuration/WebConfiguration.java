package com.tenclass.common.configuration;

import com.tenclass.common.interceptor.RequestInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * web配置类
 *
 * @author YIJIUE
 * @since 1.0.0
 */
@Configuration
@ConditionalOnMissingBean(name = "customWebConfiguration")
public class WebConfiguration implements WebMvcConfigurer {

    /**
     * 将 /api/ 下所有接口进行请求拦截，非此前缀下默认有通过权限
     * @param registry 拦截注册器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new RequestInterceptor()).addPathPatterns("/api/**");
    }

}
