package com.tenclass.common.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tenclass.common.interceptor.FeignDecodeInterceptor;
import com.tenclass.common.interceptor.FeignErrorDecoder;
import com.tenclass.common.interceptor.FeignInterceptor;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;
import feign.optionals.OptionalDecoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.ResponseEntityDecoder;
import org.springframework.context.annotation.Bean;

/**
 * feign自动配置类
 *
 * @author YIJIUE
 * @since 1.0.0
 */
public class FeignAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(name = "customFeignInterceptor")
    public FeignInterceptor feignInterceptor() {
        return new FeignInterceptor();
    }

    @Bean
    @ConditionalOnMissingBean(name = "customFeignDecoder")
    public Decoder feignDecoder(ObjectFactory<HttpMessageConverters> messageConverters) {
        return new OptionalDecoder(new ResponseEntityDecoder(new FeignDecodeInterceptor(messageConverters)));
    }

    @Bean
    @ConditionalOnMissingBean(name = "feignErrorDecoder")
    public ErrorDecoder feignErrorDecoder(ObjectMapper objectMapper) {
        return new FeignErrorDecoder(objectMapper);
    }
}
