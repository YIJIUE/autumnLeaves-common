package com.tenclass.common.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * xxl-job 配置类
 * @author YIJIUE
 * @since 1.2.2
 */
@ConfigurationProperties(prefix = "xxl.job")
public class XXLJobProperties {
    // xxl-job 任务管理器地址
    private String adminAddresses;
    // 执行器通讯TOKEN [选填]：非空时启用
    private String accessToken;
    // 执行器 application.name
    private String executorAppname;
    // 执行器地址
    private String executorAddress;
    // 执行器 ip
    private String executorIp;
    // 服务与xxl-job管理端交互的端口 默认 9999
    private int executorPort;
    // 存储执行器日志地址
    private String executorLogPath;
    // 执行器日志保存天数 [选填] ：值大于3时生效，启用执行器Log文件定期清理功能，否则不生效
    private int executorLogRetentionDays;

    public String getAdminAddresses() {
        return adminAddresses;
    }

    public void setAdminAddresses(String adminAddresses) {
        this.adminAddresses = adminAddresses;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getExecutorAppname() {
        return executorAppname;
    }

    public void setExecutorAppname(String executorAppname) {
        this.executorAppname = executorAppname;
    }

    public String getExecutorAddress() {
        return executorAddress;
    }

    public void setExecutorAddress(String executorAddress) {
        this.executorAddress = executorAddress;
    }

    public String getExecutorIp() {
        return executorIp;
    }

    public void setExecutorIp(String executorIp) {
        this.executorIp = executorIp;
    }

    public int getExecutorPort() {
        return executorPort;
    }

    public void setExecutorPort(int executorPort) {
        this.executorPort = executorPort;
    }

    public String getExecutorLogPath() {
        return executorLogPath;
    }

    public void setExecutorLogPath(String executorLogPath) {
        this.executorLogPath = executorLogPath;
    }

    public int getExecutorLogRetentionDays() {
        return executorLogRetentionDays;
    }

    public void setExecutorLogRetentionDays(int executorLogRetentionDays) {
        this.executorLogRetentionDays = executorLogRetentionDays;
    }
}
