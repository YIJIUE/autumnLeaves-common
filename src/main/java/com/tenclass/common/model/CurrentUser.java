package com.tenclass.common.model;

import org.springframework.util.Assert;

/**
 * @author YIJIUE
 * @since 1.0.0
 */
public class CurrentUser {
    private final long id;
    private final int role;
    private final String name;

    private static final ThreadLocal<CurrentUser> CURRENT = new ThreadLocal<>();

    private CurrentUser(int role, Long id, String name) {
        this.id = id;
        this.role = role;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getRole() {
        return role;
    }

    public static CurrentUser get() {
        return CURRENT.get();
    }

    public static void set(String user) {
        String[] arr = user.split(":");
        Assert.isTrue(arr.length == 3, "非法的用户信息");
        CurrentUser currentUser = new CurrentUser(Integer.parseInt(arr[0]), Long.parseLong(arr[1]), arr[2]);
        CURRENT.set(currentUser);
    }

    public static void clear() {
        CURRENT.remove();
    }

    @Override
    public String toString() {
        return role + ":" + id + ":" + name;
    }

    public static void main(String[] args) {
        String user = "0:1:";

        String[] arr = user.split(":");
        System.out.println(arr.length);
    }
}

