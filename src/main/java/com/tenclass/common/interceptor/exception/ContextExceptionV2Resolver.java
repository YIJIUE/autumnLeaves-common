package com.tenclass.common.interceptor.exception;

import cn.ilanxin.common.error.ContextExceptionV2;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.lang.NonNull;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ContextExceptionV2 响应头封装
 * @author YIJIUE
 * @since 1.3.0
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ContextExceptionV2Resolver implements HandlerExceptionResolver {
    @Override
    public ModelAndView resolveException(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response, Object handler, @NonNull Exception ex) {
        if (ex instanceof ContextExceptionV2) {
            response.setHeader("Encode-f", "-1");
        }
        return null;
    }
}
