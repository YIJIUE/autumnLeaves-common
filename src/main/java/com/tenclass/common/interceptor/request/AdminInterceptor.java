package com.tenclass.common.interceptor.request;

import cn.ilanxin.common.error.ContextExceptionV2;
import com.tenclass.common.model.CurrentUser;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

import static com.tenclass.common.constant.RequestConstant.*;

/**
 * 管理端拦截器
 *
 * @author YIJIUE
 * @since 1.0.0
 */
public class AdminInterceptor implements IRequestInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request) {
        String admin = request.getHeader(CURRENT_USER);
        if (StringUtils.isEmpty(admin) || (Integer.parseInt(admin.split(":")[0]) != ADMIN_ROLE)) {
            throw new ContextExceptionV2(401, null, "非法请求");
        }
        CurrentUser.set(URLDecoder.decode(admin, StandardCharsets.UTF_8));
        return true;
    }

}
