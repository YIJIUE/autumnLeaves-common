package com.tenclass.common.interceptor.request;


import cn.ilanxin.common.error.ContextExceptionV2;
import com.tenclass.common.model.CurrentUser;
import org.springframework.lang.NonNull;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

import static com.tenclass.common.constant.RequestConstant.CURRENT_USER;

/**
 * 用户信息解析拦截器
 *
 * @author YIJIUE
 * @since 1.0.0
 */
public class UserInterceptor implements IRequestInterceptor {

    @Override
    public boolean preHandle(@NonNull HttpServletRequest request) {
        String user = request.getHeader(CURRENT_USER);
        if (StringUtils.isEmpty(user)) {
            throw new ContextExceptionV2(401, null, "非法请求");
        } else {
            CurrentUser.set(URLDecoder.decode(user, StandardCharsets.UTF_8));
        }
        return true;
    }

}

