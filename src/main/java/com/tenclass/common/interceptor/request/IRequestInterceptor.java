package com.tenclass.common.interceptor.request;


import javax.servlet.http.HttpServletRequest;

/**
 * 请求处理器接口 所有需要拦截的方法可 impl 于此接口进行自定义拦截处理
 * 定义服务自身需要的处理器 而不是直接 impl {@link org.springframework.web.servlet.HandlerInterceptor}
 *
 * @author YIJIUE
 * @since 1.0.0
 */
public interface IRequestInterceptor {

    default boolean preHandle(HttpServletRequest request) {
        return true;
    }

}
