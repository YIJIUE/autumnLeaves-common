package com.tenclass.common.constant;

/**
 * 请求常量
 *
 * @author YIJIUE
 * @since 1.1.0
 */
public class RequestConstant {
    public static final String AUTHORIZATION = "authorization";
    public static final String CURRENT_USER = "current-user";
    public static final int ADMIN_ROLE = 0;
    public static final int USER_ROLE = 1;
    public static final String FROM_GATEWAY = "from-gateway";
}
